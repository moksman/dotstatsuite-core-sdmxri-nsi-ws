# About this Image

Official Linux-based NSI WS image, based on [siscc/sdmxri-nsi](https://hub.docker.com/r/siscc/sdmxri-nsi) image built from official repositories of Eurostat SDMXRI (NSI) SDMX, configured to be ready for usage in a [.Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/) installation.

The *siscc/sdmxri-nsi-maapi* NSI WS image provides the following features on the top of the "vanilla" version of NSI WS:
- configured to use OpenId connect authentication;
- configured to use authorization rules defined in .Stat Suite;
- includes command line tool (MAAPI tool), used to initialize/update the mapping store database objects. The tool requires an empty database with a user login defined.

This version of the siscc/sdmxri-nsi-maapi image supports MS SQL database server only.

# Environment Variables

You can use the following environment variables to configure the sdmxri-nsi-maapi service

- Authentication configuration:
  - ***auth__enabled***: true/false, controls whether OpenIdMiddlewareBuilder is activated for OpenId authentication
  - ***auth__allowAnonymous***: true/false, enables anonymous access without JWT token (please note that corresponding authorization rule(s) must to be created also, e.g. read structure and data for all users)
  - ***auth__authority***: uthority url of token issuer, e.g. "http://keycloak:8080/auth/realms/demo"
  - ***auth__clientId***: Client/application id, e.g "stat-suite"
  - ***auth__requireHttps***: true/false, is HTTPS connection to OpenId authority server require
  - ***auth__validateIssuer***: true/false, is iss (issuer) claim in JTW token should match configured authority
  - ***auth__showPii***: true/false, when set to TRUE outputs additional debug information in case of invalid token, which may contain personally identifiable information (PII)
- Authorization configuration
  - ***authorization__enabled***: true/false, determines if UserAuthorizationRulesMiddlerwareBuilder is activated for authorization. The default value for this docker image is "true", which normally should not be changed.
  - ***authorization__method***: "dotstat" - authorization uses .Stat Suite's authorization rules. The default value for this docker image is "dotstat", which normally should not be changed.
  - ***authorization__PrincipalFrom***: "context", the principal used at authorization should be taken from context object. The default value for this docker image is "context", which normally should not be changed.
  - ***mappingStore__Id__Default***: the unique identifier of the data space of the NSI WS instance (also used at authorization rules), e.g. "design"
  - ***mappingStore__Id__FromQueryParameter***: "Never", a parameter controlling whether store id (i.e. data space id) could be provided in GET Query parameter. The default value for this docker image is "Never", which normally should not be changed.
  - ***DotStatSuiteCoreCommonDbConnectionString***: the connection string to .Stat Suite's Common databse with authorization rules, e.g. 
Server=db;Database=CommonDb;User=testLoginCommon;Password=testLogin(!)Password;
- Mapping store database configuration
  - ***SQL_SERVER***: MS SQL server instance hosting the mapping store database
  - ***SQL_DATABASE***: The name of mapping store database
  - ***SQL_USER***: The application user
  - ***SQL_PASSWORD***: The passord for the application user
- MAAPI tool variables:
  - ***MA_SQL_USER***: Privileged user (e.g. sa) that has the permission to create/drop/alter db objects on the database defined by *SQL_SERVER* and *SQL_DATABASE* parameters
  - ***MA_SQL_PASSWORD***: Password of privileged user
  - ***MA_ALWAYS_RESET***: - Y/N, specifies if the mapping store database should reset (Y) or persist (N) all the changes done to the artifacts, when the container is restarted
  - ***MA_DB_VERSION***: Optional, most of the cases should not be used. Specifies the mapping store database version to be used, if not the latest is reuqired. Use this parameter only when you know what you do
- Data database connection overriding the ones stored in \[DB_CONNECTION\] table:
  - ***disseminationDbConnection__dbType***: "SqlServer/Oracle/MySQL", the type of the data database
  - ***disseminationDbConnection__connectionString***: the connection string to a data dissemination database, e.g.
Server=db;Database=DataDb;User=testLoginData;Password=testLogin(!)Password;

> The "disseminationDbConnection" parameters are optional. 
> When used, both `dbType` and `connectionString` values must be provided.
> In this case all data queries target the database defined by this connection string, the NSI WS takes data database connection parameters from these configuration parameters instead of [DB_CONNECTION] table in structure database.
- Other NSI web service variables:
  - ***INSERT_NEW_ITEM_SCHEME_VALUES***: "true/false", determines if the application is allowed to add new item scheme values, e.g. new codes to an existing code list
  - ***SENDER_ID***: the ID of the Sender put into data masages, e.g. "design - DotStat"
  - ***enableReleaseManagement***: "true/false", controls if access to PIT (point-in-time) release data allowed on this NSI WS instance (please note that corresponding user authorization rule(s) must be created also)
  - ***applyContentConstraintsOnDataQueries***: "true/false", specifies if NSI WS should apply allowed content constraints at data retrieval. The default value for this docker image is "true", which normally should not be changed.
  - ***autoDeleteMappingSets***: "true/false", specifies if NSI WS should also delete related mapping sets upon deletion of a dataflow. The default value for this docker image is `true`, which normally should not be changed.

# How to use this image

Start siscc/sdmxri-nsi-maapi (NSI WS) service instance listening on port 9000 using the latest release, where the content of structure database is preserved. 
The authentication, authorization and database configuration below can be used with the [demo docker-compose](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose) installation of .Stat Suite.

IMPORTANT NOTE: If you are using PowerShell on Windows to run these commands, use double quotes instead of single quotes.

```sh
docker run \
-e 'auth__enabled=true' \
-e 'auth__allowAnonymous=true' \
-e 'auth__authority=http://keycloak:8080/auth/realms/demo' \
-e 'auth__clientId=stat-suite ' \
-e 'auth__requireHttps=false' \
-e 'auth__validateIssuer=false' \
-e 'auth__showPii=true' \
-e 'mappingStore__Id__Default=demo-design' \
-e 'DotStatSuiteCoreCommonDbConnectionString=Server=db;Database=CommonDb;User=testLoginCommon;Password=testLogin(!)Password;' \
-e 'SQL_SERVER=db' \
-e 'SQL_DATABASE=DesignStructDb' \
-e 'SQL_USER=testLoginDesignStruct' \
-e 'SQL_PASSWORD=testLogin(!)Password' \
-e 'MA_SQL_USER=sa' \
-e 'MA_SQL_PASSWORD=My-Mssql-Pwd-123' \
-e 'MA_ALWAYS_RESET=N' \
-e 'INSERT_NEW_ITEM_SCHEME_VALUES=true' \
-e 'SENDER_ID=.Stat-v8-demo-design' \
-e 'enableReleaseManagement=true ' \
-p 9000:80 \
--network=dotstat_common_network \
siscc/sdmxri-nsi-maapi:latest
```

