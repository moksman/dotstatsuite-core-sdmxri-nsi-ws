# Content of this directory
This directory contains a shell scripts to help updating ESTAT repository mirrors. 
Prior to running these scripts you need to authenticate successfully via a web browser using your EULogin account.

## First time initialization
Use ***init.all.sh*** script to create local bare clone of ESTAT repositories (inside current directory). 
The push URLs of these repositories are changed to point to SISCC's mirror repositories.

## Update mirrors
Use ***update.all.sh***  script to update the mirror repositories. 
The script fetches all recent changes to local clone (created at *First time initialization* step) from ESTAT's Bitbucket server and pushes them to SISCC's mirror on GitLab.
